%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define MAX_SIZE 256
%define WRITE_SYSCALL 1
%define STDERR_DS 2
%define LENGTH_FAIL1 17
%define LENGTH_FAIL2 17
%define ERROR_CODE 1

section .rodata
    fail1: db 'Strign too large', 0x0A
    fail2: db '----Not found---', 0x0A

section .bss
    txt: resb MAX_SIZE

section .text

global _start

_start:
    mov rdi, txt
    mov rsi, MAX_SIZE
    call read_word
    test rax, rax
    jz .err_1
    mov rdi, rax
    mov rsi, first_word
    call find_word
    test rax, rax
    jz .err_2
    mov rdi, rax
    call print_string
    xor rdi, rdi
    jmp exit

.err_1:
    mov     rdi, fail1
    call .print_error

.err_2:
    mov     rdi, fail2

.print_error:
    mov     rsi, rdi
    mov     rax, WRITE_SYSCALL
    mov     rdi, STDERR_DS
    
    mov     rdx, LENGTH_FAIL2
    syscall
    mov rdi, ERROR_CODE
    jmp exit

