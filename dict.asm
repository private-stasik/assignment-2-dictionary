%include "lib.inc"

section .text

global find_word

find_word:
    mov r11, rdi
    mov r12, rsi
    .loop:
        test r12, r12
        jz .fin

        mov rdi, r11
        mov rsi, r12
        call string_equals
    
        .rstr:
            mov r9b, byte[r12]
            test r9b, r9b
            jnz .nonz
        
        inc r12
        test rax, rax
        jz .noneq
        ;
        add r12, 8
        mov rax, r12
        ret
    .fin:
        xor rax, rax
        ret
    .nonz:
        inc r12
        jmp .rstr
    .noneq:
        mov r12, [r12]
        jmp .loop

        